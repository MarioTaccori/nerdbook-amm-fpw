<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <title>NerdBook - Descrizione</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Mario Taccori">
        <meta name="keywords" content="NerdBook descrizione social network">
        <link rel="stylesheet" type="text/css" href="M2/style.css" media="screen">
        
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/scripts.js"></script>

    </head>
    <body>
            
        <c:set var="page" value="descrizione" scope="request"/>
        <jsp:include page="headerBar.jsp"/>
        
        <jsp:include page="sideBar.jsp"/>
        
        <div id="divBody">
            <div>
                <h1 id="titoloDescrizione">NerdBook</h1>
                <div id="sommarioDiv">    
                    <ul>
                        <li><a href="#descrizioneTeam">Il team</a>
                            <ul>
                                <li><a href="#teamChiSiamo">Chi siamo</a></li>
                                <li><a href="#teamObiettivi">Obiettivi</a></li>
                            </ul>
                        </li>

                        <li><a href="#descrizioneSocialNetwork">Il social network</a>
                            <ul>
                                <li><a href="#proNerdBook">Perchè NerdBook</a></li>
                                <li><a href="#progettiFuturi">Progetti futuri</a></li> 
                                <li><a href="#comeIscriversi">Come iscriversi</a></li>
                            </ul>                   
                    </ul>            
                </div> <!-- Fine somarioDiv -->

                <div id="teamDiv">
                    <h2 id="descrizioneTeam">Il team:</h2>

                    <div id="chiSiamoDiv">
                        <h3 id="teamChiSiamo">Chi siamo:</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue,
                           libero ut euismod rhoncus, risus lacus sodales mi, nec tempor lectus ante et elit.
                           Suspendisse sit amet lacus nulla. Cras et augue nec erat sollicitudin vulputate.
                           In lacinia urna eget lacus porttitor, eu feugiat ligula mollis. In auctor molestie porttitor.
                           Phasellus dapibus, nibh et tristique rutrum, elit sem finibus ipsum, id laoreet
                           isus arcu quis tellus. Aenean neque nulla, facilisis id dictum mattis, porta et libero.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue,
                           libero ut euismod rhoncus, risus lacus sodales mi, nec tempor lectus ante et elit.
                           Suspendisse sit amet lacus nulla. Cras et augue nec erat sollicitudin vulputate.
                           In lacinia urna eget lacus porttitor, eu feugiat ligula mollis. In auctor molestie porttitor.
                           Phasellus dapibus, nibh et tristique rutrum, elit sem finibus ipsum, id laoreet
                           isus arcu quis tellus. Aenean neque nulla, facilisis id dictum mattis, porta et libero.</p>
                    </div>

                    <div id="obiettiviDiv">
                        <h3 id="teamObiettivi">Perchè lo facciamo:</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue,
                           libero ut euismod rhoncus, risus lacus sodales mi, nec tempor lectus ante et elit.
                           Suspendisse sit amet lacus nulla. Cras et augue nec erat sollicitudin vulputate.
                           In lacinia urna eget lacus porttitor, eu feugiat ligula mollis. In auctor molestie porttitor.
                           Phasellus dapibus, nibh et tristique rutrum, elit sem finibus ipsum, id laoreet
                           isus arcu quis tellus. Aenean neque nulla, facilisis id dictum mattis, porta et libero.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue,
                           libero ut euismod rhoncus, risus lacus sodales mi, nec tempor lectus ante et elit.
                           Suspendisse sit amet lacus nulla. Cras et augue nec erat sollicitudin vulputate.
                           In lacinia urna eget lacus porttitor, eu feugiat ligula mollis. In auctor molestie porttitor.
                           Phasellus dapibus, nibh et tristique rutrum, elit sem finibus ipsum, id laoreet
                           isus arcu quis tellus. Aenean neque nulla, facilisis id dictum mattis, porta et libero.</p>
                    </div>
                </div> <!-- Fine teamDiv -->

                <div id="socialDiv">
                    <h2 id="descrizioneSocialNetwork">Il social network:</h2>

                    <div id="proNerdBookDiv">
                        <h3 id="proNerdBook">Perchè scegliere NerdBook:</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue,
                           libero ut euismod rhoncus, risus lacus sodales mi, nec tempor lectus ante et elit.
                           Suspendisse sit amet lacus nulla. Cras et augue nec erat sollicitudin vulputate.
                           In lacinia urna eget lacus porttitor, eu feugiat ligula mollis. In auctor molestie porttitor.
                           Phasellus dapibus, nibh et tristique rutrum, elit sem finibus ipsum, id laoreet
                           isus arcu quis tellus. Aenean neque nulla, facilisis id dictum mattis, porta et libero.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue,
                           libero ut euismod rhoncus, risus lacus sodales mi, nec tempor lectus ante et elit.
                           Suspendisse sit amet lacus nulla. Cras et augue nec erat sollicitudin vulputate.
                           In lacinia urna eget lacus porttitor, eu feugiat ligula mollis. In auctor molestie porttitor.
                           Phasellus dapibus, nibh et tristique rutrum, elit sem finibus ipsum, id laoreet
                           isus arcu quis tellus. Aenean neque nulla, facilisis id dictum mattis, porta et libero.</p>
                    </div>

                    <div id="progettiFuturiDiv">
                        <h3 id="progettiFuturi">Progetti futuri</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue,
                           libero ut euismod rhoncus, risus lacus sodales mi, nec tempor lectus ante et elit.
                           Suspendisse sit amet lacus nulla. Cras et augue nec erat sollicitudin vulputate.
                           In lacinia urna eget lacus porttitor, eu feugiat ligula mollis. In auctor molestie porttitor.
                           Phasellus dapibus, nibh et tristique rutrum, elit sem finibus ipsum, id laoreet
                           isus arcu quis tellus. Aenean neque nulla, facilisis id dictum mattis, porta et libero.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue,
                           libero ut euismod rhoncus, risus lacus sodales mi, nec tempor lectus ante et elit.
                           Suspendisse sit amet lacus nulla. Cras et augue nec erat sollicitudin vulputate.
                           In lacinia urna eget lacus porttitor, eu feugiat ligula mollis. In auctor molestie porttitor.
                           Phasellus dapibus, nibh et tristique rutrum, elit sem finibus ipsum, id laoreet
                           isus arcu quis tellus. Aenean neque nulla, facilisis id dictum mattis, porta et libero.</p>
                    </div>     

                    <div id="iscrizioneDiv">
                        <h3 id="comeIscriversi">Come iscriversi:</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue,
                           libero ut euismod rhoncus, risus lacus sodales mi, nec tempor lectus ante et elit.
                           Suspendisse sit amet lacus nulla. Cras et augue nec erat sollicitudin vulputate.
                           In lacinia urna eget lacus porttitor, eu feugiat ligula mollis. In auctor molestie porttitor.
                           Phasellus dapibus, nibh et tristique rutrum, elit sem finibus ipsum, id laoreet
                           isus arcu quis tellus. Aenean neque nulla, facilisis id dictum mattis, porta et libero.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue,
                           libero ut euismod rhoncus, risus lacus sodales mi, nec tempor lectus ante et elit.
                           Suspendisse sit amet lacus nulla. Cras et augue nec erat sollicitudin vulputate.
                           In lacinia urna eget lacus porttitor, eu feugiat ligula mollis. In auctor molestie porttitor.
                           Phasellus dapibus, nibh et tristique rutrum, elit sem finibus ipsum, id laoreet
                           isus arcu quis tellus. Aenean neque nulla, facilisis id dictum mattis, porta et libero.</p>                    
                    </div>
                </div> <!-- Fine socialDiv -->
            </div>
        </div> <!-- Fine divBody -->
    </body>
</html>
