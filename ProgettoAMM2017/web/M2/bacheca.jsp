<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>
<!DOCTYPE html>

<html>
    <head>
        <title>Bacheca di ${owner.nome}</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Mario Taccori">
        <meta name="keywords" content="NerdBook bacheca social network">
        <link rel="stylesheet" type="text/css" href="M2/style.css" media="screen">
        
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/scripts.js"></script>
    </head>
    <body>
        
        <c:set var="page" value="bacheca" scope="request"/>
        <jsp:include page="headerBar.jsp"/>
        
        <jsp:include page="sideBar.jsp"/>
        
        <div id="divBody">
            <c:choose>
                <c:when test="${sessionScope.loggedIn==true}">             
                    <c:choose>
                        <c:when test="${(isFriend != null && isFriend == true) || (isSubscribed != null && isSubscribed == true) }">
                            <c:if test="${ownerType!='group' && owner.frasePresentazione!=null}">
                                <div id="presentazione">
                                    <c:set var="picSubject" value="selfPresentazione" scope="request" />
                                    <jsp:include page="fotoProfilo.jsp" />
                                    <div><p>${owner.frasePresentazione}</p></div>
                                </div>
                            </c:if>
                            
                            <c:choose>
                                <c:when test="${postState != null && postState == 'created'}">
                                    <c:set var="notificationType" value="newPostCreation" scope="request" />
                                    <c:set var="notificationValue" value="newPostCreationOk" scope="request" />
                                    <jsp:include page="notifications.jsp" />
                                </c:when>
                                
                                <c:when test="${postState != null && postState == 'error'}">
                                    <c:set var="notificationType" value="newPostCreation" scope="request" />
                                    <c:set var="notificationValue" value="newPostCreationError" scope="request" />
                                    <jsp:include page="notifications.jsp" />
                                </c:when>
                            </c:choose>

                            <c:choose>
                                <c:when test="${confirmRequired==null || confirmRequired==false}">
                                    <div id="nuovoPostDiv">
                                        <form id="formNuovoPost" action="bacheca.html" method="post">
                                            <div id="inputsDiv"> 
                                                <div>
                                                    <label for="contenuto"></label>
                                                    <input type="text" name="contenuto" id="contenuto" value="Testo nuovo post">
                                                </div>
                                                <div>
                                                    <label for="allegato"></label>
                                                    <input type="url" name="allegato" id="allegato" value="URL allegato (opzionale, cancella se non necessario)">
                                                </div>
                                            </div>

                                            <div id="radiosDiv">
                                                <div class="filler"></div>
                                                <div>
                                                    <input type="radio" name="postType" id="postTypeTextRadio" value="testo" checked="checked">
                                                    <label for="postTypeTextRadio">Testo</label>
                                                    <input type="radio" name="postType" id="postTypeImmagineRadio" value="immagine">
                                                    <label for="postTypeImmagineRadio">Immagine</label>
                                                    <input type="radio" name="postType" id="postTypeLinkRadio" value="link">
                                                    <label for="postTypeLinkRadio">Link</label>
                                                </div>
                                            </div>

                                            <input type="hidden" name="owner" value="${owner.id}">
                                            <input type="hidden" name="ownerType" value="${ownerType}">
                                            <input type="hidden" name="action" value="newPost">
                                            <input type="hidden" name="confirmRequired" value="true">

                                            <div id="buttonDiv">
                                                <div class="filler"></div>
                                                <button type="submit" form="formNuovoPost">Crea post</button>
                                            </div>
                                        </form> 
                                    </div>
                                </c:when>

                                <c:when test="${confirmRequired!=null && confirmRequired==true}">
                                    <div id="nuovoPostDiv">
                                        <div id="riepilogoDiv">
                                            <p>Riepilogo dati inseriti:</p>
                                            <ul>
                                                <li>Autore: "${requestScope.user.nome} ${requestScope.user.cognome}"</li>
                                                <li>
                                                    Proprietario della bacheca:
                                                    <c:choose>
                                                        <c:when test="${previewPost.tipoDestinazione == 'BACHECA'}">
                                                            "${owner.nome} ${owner.cognome}"
                                                        </c:when>

                                                        <c:when test="${previewPost.tipoDestinazione == 'GRUPPO'}">
                                                            Gruppo "${owner.nome}"
                                                        </c:when>

                                                    </c:choose>                                        
                                                </li>
                                                <li>Testo: "${previewPost.contenuto}"</li>
                                                <li>URL: "${previewPost.attachedUrl}"</li>
                                                <li>Tipo: "${previewPost.tipoPost}"</li>                    
                                            </ul>
                                        </div>

                                        <div id="previewPostDiv">    
                                            <p>Preview:</p>
                                            <c:set var="post" value="${previewPost}" scope="request" />
                                            <jsp:include page="post.jsp" />     
                                        </div>

                                        <div id="choiceDiv">
                                            <div>
                                                <form id="newPostForm" action="bacheca.html" method="post">
                                                    <!-- (FATTO) AGGIUNGERE I FORM HIDDEN PER RIMANDARE I DATI DEL NUOVO POST PER SALVARLO IN DB -->
                                                    <input type="hidden" name="contenuto" value="${previewPost.contenuto}">
                                                    <input type="hidden" name="allegato" value="${previewPost.attachedUrl}">
                                                    <input type="hidden" name="postType" value="${previewPost.tipoPost}">
                                                    <input type="hidden" name="owner" value="${owner.id}">
                                                    <input type="hidden" name="ownerType" value="${ownerType}">
                                                 
                                                    <div>
                                                        <button type="submit" form="newPostForm" name="action" value="confirmNewPost">Conferma</button>
                                                        <button type="submit" form="newPostForm" name="action" value="cancelNewPost">Annulla</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>        
                                </c:when>                      
                            </c:choose>  

                            <div id="posts"> <!-- Sezione dei post -->
                                <c:forEach var="postTmp" items="${listaPost}">
                                    <c:set var="post" value="${postTmp}" scope="request" />
                                    <jsp:include page="post.jsp" />                      
                                </c:forEach>
                            </div> <!-- Chiusura sezione dei post -->

                        </c:when>
                            
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${isSubscribed!=null}">
                                    <form id="newSubscriptionForm" action="bacheca.html" method="POST">
                                        <input type="hidden" name="action" value="registerSubscription">
                                        <input type="hidden" name="owner" value="${owner.id}">
                                        <input type="hidden" name="ownerType" value="${ownerType}">
                                        <button type="submit" form="newSubscriptionForm">Iscriviti al gruppo ${owner.nome}</button>
                                    </form>
                                </c:when>
                                <c:when test="${isFriend!=null}">
                                    <form id="newFriendshipForm" action="bacheca.html" method="POST">
                                        <input type="hidden" name="action" value="registerFriendship">
                                        <input type="hidden" name="owner" value="${owner.id}">
                                        <input type="hidden" name="ownerType" value="${ownerType}">
                                        <button type="submit" form="newFriendshipForm">Richiedi amicizia a ${owner.nome} ${owner.cognome}</button>
                                    </form>
                                </c:when>
                            </c:choose>
                        </c:otherwise>
                        
                        </c:choose>
                </c:when>
                                       
                <c:otherwise>
                    <c:set var="notificationType" value="accessDenied" scope="request" />
                    <c:set var="notificationValue" value="accessDenied" scope="request" />
                    <jsp:include page="notifications.jsp" />
                </c:otherwise>
                    
            </c:choose>
        </div><!-- Fine divBody -->
        
        <div class="clear"></div>
    </body>
</html>
